package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private PizzaStore depokPizzaStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testDepokCheesePizza() {

        pizza = depokPizzaStore.orderPizza("cheese");
        Assert.assertEquals(pizza.toString(),
                "---- Cheese Pizza ala Depok that is delicious but deep down isn't ----\n"
                        + "Dough made from dried soil and dirt mixture\n"
                        + "Raw Sauce that's not hot\n"
                        + "Cheap, stinky, stolen cheese\n");

    }

    @Test
    public void testDepokClamPizza() {
        pizza = depokPizzaStore.orderPizza("clam");
        Assert.assertEquals(pizza.toString(),
                "---- Depok Clam Pizza that will bring back "
                        + "your deepest darkest past memories ----\n"
                        + "Dough made from dried soil and dirt mixture\n"
                        + "Raw Sauce that's not hot\n"
                        + "Cheap, stinky, stolen cheese\n"
                        + "A year-old canned clams produced by multi-nation capitalist industry\n");
    }

    @Test
    public void testDepokVeggiePizza() {
        pizza = depokPizzaStore.orderPizza("veggie");
        Assert.assertEquals(pizza.toString(),
                "---- Vegetable Pizza that nobody wants but still"
                        + " order because of their group's agenda ----\n"
                        + "Dough made from dried soil and dirt mixture\n"
                        + "Raw Sauce that's not hot\n"
                        + "Cheap, stinky, stolen cheese\n"
                        + "Mushrooms, Red Pepper, Meat that's "
                        + "not actually meat made from vegetables to fulfill\n"
                        + "veggies dream and lust from meat existence in their life\n");
    }


}
