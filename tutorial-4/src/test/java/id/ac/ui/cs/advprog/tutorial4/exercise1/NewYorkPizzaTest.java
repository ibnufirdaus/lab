package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaTest {
    private PizzaStore newYorkPizzaStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testNyCheesePizza() {
        pizza = newYorkPizzaStore.orderPizza("cheese");
        Assert.assertEquals(pizza.toString(),
                "---- New York Style Cheese Pizza ----\n"
                        + "Thin Crust Dough\n"
                        + "Marinara Sauce\n"
                        + "Reggiano Cheese\n");
    }

    @Test
    public void testNyClamPizza() {
        pizza = newYorkPizzaStore.orderPizza("clam");
        Assert.assertEquals(pizza.toString(),
                "---- New York Style Clam Pizza ----\n"
                        + "Thin Crust Dough\n"
                        + "Marinara Sauce\n"
                        + "Reggiano Cheese\n"
                        + "Fresh Clams from Long Island Sound\n");
    }

    @Test
    public void testNyVeggiePizza() {
        pizza = newYorkPizzaStore.orderPizza("veggie");
        Assert.assertEquals(pizza.toString(),
                "---- New York Style Veggie Pizza ----\n"
                        + "Thin Crust Dough\n"
                        + "Marinara Sauce\n"
                        + "Reggiano Cheese\n"
                        + "Garlic, Onion, Mushrooms, Red Pepper\n");
    }
}
