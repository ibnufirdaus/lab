package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class CheapCheese implements Cheese {

    @Override
    public String toString() {
        return "Cheap, stinky, stolen cheese";
    }
}
