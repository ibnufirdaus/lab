package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class CannedClams implements Clams {

    @Override
    public String toString() {
        return "A year-old canned clams produced by multi-nation capitalist industry";
    }
}
