package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class VegetableMeat implements Veggies {

    @Override
    public String toString() {
        return "Meat that's not actually meat made from vegetables to fulfill\n"
                + "veggies dream and lust from meat existence in their life";
    }
}
