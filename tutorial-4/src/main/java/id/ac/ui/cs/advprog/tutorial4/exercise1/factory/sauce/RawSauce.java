package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class RawSauce implements Sauce {

    @Override
    public String toString() {
        return "Raw Sauce that's not hot";
    }
}
