package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SoilDough implements Dough {

    @Override
    public String toString() {
        return "Dough made from dried soil and dirt mixture";
    }
}
