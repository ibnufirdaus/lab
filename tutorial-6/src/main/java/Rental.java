import java.util.Iterator;
import javax.swing.text.html.HTMLDocument;

class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public static double calculatePrice(Iterator<Rental> iterator) {
        double price = 0;

        while (iterator.hasNext()) {
            Rental rental = iterator.next();
            price += getPrice(rental);
        }

        return price;
    }

    public static double getPrice(Rental rental) {
        double price = 0;
        if (rental.getMovie().getPriceCode() == Movie.REGULAR) {
            price += 2;
            if (rental.daysRented > 2) {
                price += (rental.daysRented - 2) * 1.5;
            }
        } else if (rental.getMovie().getPriceCode() == Movie.NEW_RELEASE) {
            price += rental.daysRented * 3;
        } else if (rental.getMovie().getPriceCode() == Movie.CHILDREN) {
            price += 1.5;
            if (rental.daysRented > 3) {
                price += (rental.daysRented - 3) * 1.5;
            }
        }

        return price;
    }

    public static int calculatePoints(Iterator<Rental> iterator) {
        int points = 0;

        while (iterator.hasNext()) {
            Rental rental = iterator.next();

            points++;
            if ((rental.getMovie().getPriceCode() == Movie.NEW_RELEASE)
                    && rental.getDaysRented() > 1) {
                points++;
            }
        }

        return points;
    }
}