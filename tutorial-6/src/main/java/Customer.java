import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = Rental.getPrice(each);

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(thisAmount) + "\n";
        }


        // Add footer lines
        result += "Amount owed is "
                + String.valueOf(Rental.calculatePrice(rentals.iterator())) + "\n";
        result += "You earned "
                + String.valueOf(Rental.calculatePoints(rentals.iterator()))
                + " frequent renter points";

        return result;
    }






}