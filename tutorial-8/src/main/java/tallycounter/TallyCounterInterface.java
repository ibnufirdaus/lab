package tallycounter;

public interface TallyCounterInterface {

    public void increment();

    public void decrement();

    public int value();

}
