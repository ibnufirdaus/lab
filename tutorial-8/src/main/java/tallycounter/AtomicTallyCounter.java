package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter implements TallyCounterInterface {

    AtomicInteger counter;

    AtomicTallyCounter() {
        counter = new AtomicInteger(0);
    }

    @Override
    public void increment() {
        counter.incrementAndGet();
    }

    @Override
    public void decrement() {
        counter.decrementAndGet();
    }

    @Override
    public int value() {
        return counter.get();
    }
}
