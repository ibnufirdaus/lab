package tutorial.javari;

public class ErrorObject extends Throwable {
    private String message;

    ErrorObject(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
