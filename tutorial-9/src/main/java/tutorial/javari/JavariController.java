package tutorial.javari;

import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class JavariController {


    @RequestMapping(value = "/javari/{id}", method = GET)
    public Animal getSingleAnimal(@PathVariable int id) throws ErrorObject {
        setupData();

        for (Animal animal : JavariModel.animals) {
            if (animal.getId() == id) {
                return animal;
            }
        }

        throw new ErrorObject("Animal with ID : " + id + ", Not Found !");
    }

    @RequestMapping(value = "/javari", method = GET)
    public List<Animal> getAllAnimal() throws ErrorObject {
        setupData();
        if(JavariModel.animals.size() > 0) {
            return JavariModel.animals;
        }
        throw new ErrorObject("Animal List is Empty");
    }

    @DeleteMapping(value = "/javari/{id}")
    public void deleteAnimal(@PathVariable int id) {
        setupData();

        JavariModel.deleteFromList(id);

    }

    @PostMapping(value = "/javari/{json}")
    public void addAnimal(@PathVariable String json) {

    }

    private void setupData() {
        if (!JavariModel.dataInitiated) {
            JavariModel.fillAnimalList();
            JavariModel.dataInitiated = true;
        }
    }
}
