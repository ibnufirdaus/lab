package tutorial.javari;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JavariModel {

    public static List<Animal> animals = new ArrayList<Animal>();
    public static boolean dataInitiated = false;

    public static void refillAnimalList() {
        animals = new ArrayList<Animal>();
        fillAnimalList();
    }

    public static void fillAnimalList() {
        try {
            FileReader fr = new FileReader("animal_records.csv");
            BufferedReader reader = new BufferedReader(fr);

            String lines = reader.readLine();
            String[] splittedLines;
            while (!lines.equals("")) {
                splittedLines = lines.split(",");

                int id = Integer.parseInt(splittedLines[0]);
                String type = splittedLines[1];
                String name = splittedLines[2];
                Gender gender = Gender.parseGender(splittedLines[3]);
                Double length = Double.parseDouble(splittedLines[4]);
                Double weight = Double.parseDouble(splittedLines[5]);
                Condition condition = Condition.parseCondition(splittedLines[6]);

                animals.add(new Animal(id, type, name, gender, length, weight, condition));

                lines = reader.readLine();
            }
            reader.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFromList(int id) {
        try {
            FileReader fr = new FileReader("animal_records.csv");
            BufferedReader reader = new BufferedReader(fr);

            String line = reader.readLine();
            StringBuilder sb = new StringBuilder();

            while (line != null) {
                int animalId = Integer.parseInt(line.split(",")[0]);

                if (animalId != id) {
                    sb.append(line + "\n");
                }

                line = reader.readLine();
            }

            fr.close();
            reader.close();

            FileWriter fw = new FileWriter("animal_records.csv");
            BufferedWriter writer = new BufferedWriter(fw);

            writer.write(sb.toString());
            writer.flush();

            fw.close();
            writer.close();

            refillAnimalList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
