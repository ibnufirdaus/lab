package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Filling {

    Food food;

    public Tomato(Food food) {
        this.food = food;
        this.description = "tomato";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.5;
    }
}
