package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {

        if (salary < 30000.0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
