package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.Iterator;

public class ExampleCompany {
    public static void main(String[] args) {
        Company company = new Company();

        Ceo ceo = new Ceo("CEO",950000);
        Cto cto = new Cto("CTO",850000);

        BackendProgrammer backendProgrammer = new BackendProgrammer("BP",90000);
        FrontendProgrammer frontendProgrammer = new FrontendProgrammer("FP",85000);
        NetworkExpert networkExpert = new NetworkExpert("NE", 50000);
        SecurityExpert securityExpert = new SecurityExpert("SE", 70000);
        UiUxDesigner uiUxDesigner = new UiUxDesigner("UI/UX",90000);

        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(backendProgrammer);
        company.addEmployee(frontendProgrammer);
        company.addEmployee(networkExpert);
        company.addEmployee(securityExpert);
        company.addEmployee(uiUxDesigner);

        Iterator<Employees> iterator = company.getAllEmployees().iterator();

        while (iterator.hasNext()) {
            Employees employees = iterator.next();
            String profil = employees.name + " " + employees.role + " " + employees.salary;
            System.out.println(profil);
        }

        System.out.println("Company Expenses : " + Double.toString(company.getNetSalaries()));
    }

}
