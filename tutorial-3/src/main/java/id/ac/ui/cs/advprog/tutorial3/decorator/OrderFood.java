package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class OrderFood {

    public static void main(String[] args) {


        // Memesan Crusty Sandwich dengan keju, chili sauce, tomat, dan chicken
        Food crustySandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        crustySandwich = FillingDecorator.CHEESE.addFillingToBread(crustySandwich);
        crustySandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(crustySandwich);
        crustySandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(crustySandwich);

        String checkOut;
        checkOut = "Harga " + crustySandwich.getDescription() + " = ";
        checkOut += Double.toString(crustySandwich.cost());
        System.out.println(checkOut);

        // Memesan No Crust Sandwich dengan beef, cucumber, dan lettuce
        Food noCrustSandwich = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        noCrustSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(noCrustSandwich);
        noCrustSandwich = FillingDecorator.CUCUMBER.addFillingToBread(noCrustSandwich);
        noCrustSandwich = FillingDecorator.LETTUCE.addFillingToBread(noCrustSandwich);

        checkOut = "Harga " + noCrustSandwich.getDescription() + " = ";
        checkOut += Double.toString(noCrustSandwich.cost());
        System.out.println(checkOut);

        Food thickBunBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurger = FillingDecorator.TOMATO.addFillingToBread(thickBunBurger);
        thickBunBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(thickBunBurger);

        checkOut = "Harga " + thickBunBurger.getDescription() + " = ";
        checkOut += Double.toString(thickBunBurger.cost());
        System.out.println(checkOut);

        Food thinBunBurger = BreadProducer.THIN_BUN.createBreadToBeFilled();

        checkOut = "Harga " + thinBunBurger.getDescription() + " = ";
        checkOut += Double.toString(thinBunBurger.cost());
        System.out.println(checkOut);

    }

}
