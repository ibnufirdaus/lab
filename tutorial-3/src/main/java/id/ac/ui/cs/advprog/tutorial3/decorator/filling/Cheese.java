package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Filling {

    Food food;

    public Cheese(Food food) {
        this.food = food;
        this.description = "cheese";
    }

    @Override
    public double cost() {
        return food.cost() + 2;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description;
    }
}
