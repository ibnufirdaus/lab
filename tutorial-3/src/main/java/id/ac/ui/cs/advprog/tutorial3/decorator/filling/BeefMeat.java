package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Filling {
    Food food;

    public BeefMeat(Food food) {
        //TODO Implement
        this.food = food;
        this.description = "beef meat";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + ", adding " + description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 6;
    }
}
