package applicant;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.function.Predicate;

import static applicant.Applicant.evaluate;

public class ApplicantTest {

    Applicant applicant;
    Predicate<Applicant> qualifiedEvaluator;
    Predicate<Applicant> creditEvaluator;
    Predicate<Applicant> employmentEvaluator;
    Predicate<Applicant> criminalEvaluator;

    @Before
    public void setUp() {
        applicant = new Applicant();

        qualifiedEvaluator =
                Applicant::isCredible;

        creditEvaluator =
            anApplicant -> anApplicant.getCreditScore() > 600;

        employmentEvaluator =
            anApplicant -> anApplicant.getEmploymentYears() > 0;

        criminalEvaluator =
            anApplicant -> !anApplicant.hasCriminalRecord();
    }

    @Test
    public void testAccepted() {
        assertTrue(Applicant.evaluate(applicant,creditEvaluator));

    }

    @Test
    public void testRejected() {
        assertFalse(Applicant.evaluate(applicant,
                criminalEvaluator.and(creditEvaluator)
                        .and(employmentEvaluator).and(qualifiedEvaluator)));

    }
}
