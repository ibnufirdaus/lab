import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class ScoreGroupingTest {

    @Test
    public void testGroupBy() {

        Map<String, Integer> scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        assertEquals("{11=[Charlie, Foxtrot], 12=[Alice], 15=[Emi, Bob, Delta]}",
                ScoreGrouping.groupByScores(scores).toString());

    }
}