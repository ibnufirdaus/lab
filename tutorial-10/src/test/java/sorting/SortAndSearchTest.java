package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    int[] originalArr = {6,5,4,3,2,1};
    int[] processArr;
    int[] correctArr = {1,2,3,4,5,6};

    @Test
    public void testFirstAlgorithm() {
        processArr = originalArr.clone();
        assertEquals(Finder.slowSearch(processArr,3),3);

        processArr = Sorter.slowSort(processArr);
        contentChecker(processArr,correctArr);
    }

    @Test
    public void testSecondAlgorithm() {
        processArr = originalArr.clone();
        assertEquals(Finder.slowLessSearch(processArr,3),3);

        processArr = Sorter.quickSort(processArr);
        contentChecker(processArr,correctArr);

        assertEquals(Finder.binarySearch(processArr,3), 3);
    }

    private void contentChecker(int[] arr1, int[] arr2) {
        for (int a = 0; a < arr1.length; a++) {
            assertEquals(arr1[a],arr2[a]);
        }
    }
}
