package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {

        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
            model.addAttribute("welcome", "Here's your CV !");
        } else {
            model.addAttribute("title",name + ", I hope you interested to hire me");
            model.addAttribute("welcome", "Welcome to my CV, " + name + "!");
        }

        return "greeting";
    }

}
